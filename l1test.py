from codes import ImagesRecursives

image_traitant = ImagesRecursives()
image_traitant.upload_image()

# Récupérer le nom de l'image uploadée
uploaded_filenames = list (image_traitant.uploaded_files.keys ())
if uploaded_filenames:
    image_filename = uploaded_filenames[0]

# Utilisation de la fonction load_image et decouper_image
image = image_traitant.load_image(image_filename)
image = image.convert('RGB')

nouvelle_image = image_traitant.decouper_image(image, ordre = 2)
nouvelle_image.show()
# from google.colab import files  # Importation de la fonctionnalité de téléchargement de fichiers
import tkinter as tk
from tkinter import filedialog
from PIL import Image # Importation de la classe Image du module PIL (Python Imaging Library)
import math  # Importation du module math pour les calculs mathématiques

class ImagesRecursives:
    def __init__(self):
        self.uploaded_files = None  
        # Initialisation de la variable pour stocker les fichiers téléchargés

    
    def upload_image(self):
        root = tk.Tk()
        root.withdraw()  # Masquer la fenêtre principale de tkinter
        filepath = filedialog.askopenfilename()  # Ouvrir une boîte de dialogue pour sélectionner un fichier
        root.destroy()  # Détruire la fenêtre après avoir sélectionné le fichier
        if filepath:
            self.uploaded_files = {filepath: Image.open(filepath)}  # Stocker le fichier téléchargé
            print('Le fichier "{name}" a été téléchargé'.format(name=filepath))

    def load_image(self, filename):
        return Image.open(filename)  # Charger l'image à partir du nom de fichier passé en paramètre

    def resize_image(self, image):
        largeur, hauteur = image.size  # Obtient les dimensions de l'image
        dimension = max(largeur, hauteur)  # Prend la dimension la plus grande parmi la largeur et la hauteur
        nouvelle_dimension = 2 ** math.ceil(math.log2(dimension))  # Arrondit la dimension à la puissance de 2 la plus proche
        resized_image = image.resize((nouvelle_dimension, nouvelle_dimension))  # Redimensionne l'image à la nouvelle dimension
        return resized_image

    def decouper_image(self, image, ordre):
        image = self.resize_image(image)  # Redimensionne l'image en utilisant la méthode créée avant
        largeur, hauteur = image.size  # Obtient les dimensions de l'image redimensionnée
        blocs = 2 ** ordre  # Calcule le nombre de blocs en fonction de l'ordre spécifié
        bloc_largeur = largeur // blocs  # Calcule la largeur de chaque bloc
        bloc_hauteur = hauteur // blocs  # Calcule la hauteur de chaque bloc
        nouvelles_couleurs = []  # Initialise une liste pour stocker les couleurs moyennes de chaque bloc

        for x in range(blocs):
            for y in range(blocs): 
                # Découpe un bloc de l'image à partir des coordonnées spécifiées
                bloc = image.crop((x * bloc_largeur, y * bloc_hauteur, (x + 1) * bloc_largeur, (y + 1) * bloc_hauteur))
                # Réduit la taille du bloc à 1x1 pixel et obtient sa couleur moyenne
                couleur_moyenne = bloc.getpixel((0, 0))
                nouvelles_couleurs.append(couleur_moyenne)  # Ajoute la couleur moyenne à la liste

        # Crée une nouvelle image avec les dimensions de l'image d'origine
        nouvelle_image = Image.new("RGB", (largeur, hauteur))

        index = 0
        for x in range(blocs):
            for y in range(blocs):
                couleur = nouvelles_couleurs[index]  # Récupère la couleur moyenne correspondante
                index = index + 1
                # Colle la couleur moyenne dans le bloc de la nouvelle image
                nouvelle_image.paste(couleur, (x * bloc_largeur, y * bloc_hauteur, (x + 1) * bloc_largeur, (y + 1) * bloc_hauteur))

        return nouvelle_image  # Renvoie la nouvelle image découpée